﻿using System;
using System.Drawing;

namespace SplashScreen
{
    class XmlAccess
    {
        static private string szFileName = "";
        static private System.Data.DataSet dsSplashScreen = null;

        static public string XmlFile
        {
            set
            {
                szFileName = value;
                dsSplashScreen = new System.Data.DataSet("SplashScreen");
                dsSplashScreen.ReadXml(szFileName);
            }
        }

        #region "Private Helper Methods"
        static private string FontToString(Font fnt)
        {
            string sFont = "FamilyName=" + fnt.FontFamily.Name + ";";
            sFont += "Size=" + fnt.Size.ToString(System.Globalization.NumberFormatInfo.InvariantInfo) + ";";
            sFont += "Bold=" + (fnt.Bold ? "1" : "0") + ";";
            sFont += "Italic=" + (fnt.Italic ? "1" : "0") + ";";
            sFont += "StrikeOut=" + (fnt.Strikeout ? "1" : "0") + ";";
            sFont += "UnderLine=" + (fnt.Underline ? "1" : "0");

            return sFont;
        }

        static private Font StringToFont(string sFont)
        {
            string[] parts = sFont.Split(new char[] { '=', ';' });

            FontStyle fs = (parts[5] == "1") ? FontStyle.Bold : FontStyle.Regular;
            if (parts[7] == "1")
                fs |= FontStyle.Italic;
            if (parts[9] == "1")
                fs |= FontStyle.Strikeout;
            if (parts[11] == "1")
                fs |= FontStyle.Underline;

            return new Font(parts[1], (float)Convert.ToDouble(parts[3], System.Globalization.NumberFormatInfo.InvariantInfo), fs);
        }

        static private Color StringToColor(string str)
        {
            byte alpha = Convert.ToByte(str.Substring(0, 2), 16);
            byte red = Convert.ToByte(str.Substring(2, 2), 16);
            byte green = Convert.ToByte(str.Substring(4, 2), 16);
            byte blue = Convert.ToByte(str.Substring(6, 2), 16);
            return Color.FromArgb(alpha, red, green, blue);
        }

        static private string ColorToString(Color clr)
        {
            string str = clr.A.ToString("X2");
            str += clr.R.ToString("X2");
            str += clr.G.ToString("X2");
            str += clr.B.ToString("X2");
            return str;
        }

        static private string ColorToString(int alpha, Color clr)
        {
            string str = alpha.ToString("X2");
            str += clr.R.ToString("X2");
            str += clr.G.ToString("X2");
            str += clr.B.ToString("X2");
            return str;
        }

        static private Point StringToPoint(string str)
        {
            string[] parts = str.Trim().Split(new char[] { ',' });
            return new Point(Convert.ToInt32(parts[0]), Convert.ToInt32(parts[1]));
        }

        static private string PointToString(Point pnt)
        {
            return pnt.X.ToString() + "," + pnt.Y.ToString();
        }

        static private Size StringToSize(string str)
        {
            string[] parts = str.Trim().Split(new char[] { 'x' });
            return new Size(Convert.ToInt32(parts[0]), Convert.ToInt32(parts[1]));
        }

        static private string SizeToString(Size sz)
        {
            return sz.Width.ToString() + "x" + sz.Height.ToString();
        }
        #endregion

        static public string GetString(string key)
        {
            if (dsSplashScreen == null)
                return "";

            try
            {
                return dsSplashScreen.Tables["SplashScreen"].Rows[0][key].ToString();
            }
            catch (Exception)
            {
                return "";
            }
        }

        static public double GetDouble(string key)
        {
            if (dsSplashScreen == null)
                return 0;

            try
            {
                return Convert.ToDouble(dsSplashScreen.Tables["SplashScreen"].Rows[0][key], System.Globalization.NumberFormatInfo.InvariantInfo);
            }
            catch (Exception)
            {
                return 0;
            }
        }

        static public int GetInt(string key)
        {
            if (dsSplashScreen == null)
                return 0;

            try
            {
                return Convert.ToInt32(dsSplashScreen.Tables["SplashScreen"].Rows[0][key]);
            }
            catch (Exception)
            {
                return 0;
            }
        }

        static public Point GetPoint(string key)
        {
            if (dsSplashScreen == null)
                return new Point(1,1);

            try
            {
                return StringToPoint(dsSplashScreen.Tables["SplashScreen"].Rows[0][key].ToString());
            }
            catch (Exception)
            {
                return new Point(1, 1);
            }
        }

        static public Size GetSize(string key)
        {
            if (dsSplashScreen == null)
                return new Size(1,1);

            try
            {
                return StringToSize(dsSplashScreen.Tables["SplashScreen"].Rows[0][key].ToString());
            }
            catch (Exception)
            {
                return new Size(1, 1);
            }
        }

        static public Color GetColor(string key)
        {
            if (dsSplashScreen == null)
                return Color.Black;

            try
            {
                return StringToColor(dsSplashScreen.Tables["SplashScreen"].Rows[0][key].ToString());
            }
            catch (Exception)
            {
                return Color.Black;
            }
        }

        static public Font GetFont(string key)
        {
            if (dsSplashScreen == null)
                return StringToFont("FamilyName=Microsoft Sans Serif;Size=8.25;Bold=0;Italic=0;StrikeOut=0;UnderLine=0");

            try
            {
                return StringToFont(dsSplashScreen.Tables["SplashScreen"].Rows[0][key].ToString());
            }
            catch (Exception)
            {
                return StringToFont("FamilyName=Microsoft Sans Serif;Size=8.25;Bold=0;Italic=0;StrikeOut=0;UnderLine=0");
            }
        }

        static public void SetString(string key, string value)
        {
            if (dsSplashScreen == null)
                return;

            try
            {
                System.Data.DataTable dt = dsSplashScreen.Tables["SplashScreen"];
                if (!dt.Columns.Contains(key))
                    dt.Columns.Add(key, System.Type.GetType("System.String"));
                    
                dt.Rows[0][key] = value;

                System.IO.File.Delete(szFileName);
                dsSplashScreen.WriteXml(szFileName);
            }
            catch (Exception)
            {
            }
        }

        static public void SetDouble(string key, double value)
        {
            if (dsSplashScreen == null)
                return;

            try
            {
                System.Data.DataTable dt = dsSplashScreen.Tables["SplashScreen"];
                if (!dt.Columns.Contains(key))
                    dt.Columns.Add(key, System.Type.GetType("System.Double"));

                dt.Rows[0][key] = value.ToString(System.Globalization.NumberFormatInfo.InvariantInfo);

                System.IO.File.Delete(szFileName);
                dsSplashScreen.WriteXml(szFileName);
            }
            catch (Exception)
            {
            }
        }
    }
}
