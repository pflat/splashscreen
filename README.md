# SplashScreen

Add a splash screen to .NET Framework WinForms applications.  
This library targets .NET Framework 2.0.

### Credits

The initial code was taken from [https://www.codeproject.com/articles/5454/a-pretty-good-splash-screen-in-c](https://www.codeproject.com/articles/5454/a-pretty-good-splash-screen-in-c), by [Tom Clement](https://www.codeproject.com/script/Membership/View.aspx?mid=383127).  
I updated the code mostly to allow images with transparent regions, and also to be more configurable in the configuration file.

#### Note

The project has been updated by some members of the CodeProject community.  
Those changes are not reflected on this code.

### Usage:

In you application, you need to add the following to your form (or main form, if you have several):

```sh
public Form1()
{
    InitializeComponent();

    SplashScreen.SplashScreen.ShowSplashScreen("SplashScreen.xml");
    SplashScreen.SplashScreen.SetStatus("Loading module 1");
    System.Threading.Thread.Sleep(1000);
    SplashScreen.SplashScreen.SetStatus("Loading module 2");
    System.Threading.Thread.Sleep(3000);
    SplashScreen.SplashScreen.SetStatus("Loading module 3");
    System.Threading.Thread.Sleep(1000);
    SplashScreen.SplashScreen.SetStatus("Loading module 4");
    System.Threading.Thread.Sleep(1000);
    SplashScreen.SplashScreen.SetStatus("Loading module 5");
    System.Threading.Thread.Sleep(1750);
    SplashScreen.SplashScreen.SetStatus("Loading module 6");
    System.Threading.Thread.Sleep(250);
    SplashScreen.SplashScreen.SetStatus("Done");
}

protected override void OnLoad(EventArgs e)
{
    base.OnLoad(e);

    SplashScreen.SplashScreen.CloseSplashScreen(this.Handle);
    this.Activate();
    this.BringToFront();
}
```

You need to provide the image to use in the SplashScreen, and the SplashScreen configuration file (by default SplashScreen.xml).  
This project provides a sample configuration file.
