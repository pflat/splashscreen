﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using System.Threading;

//  Taken from:
//  https://www.codeproject.com/articles/5454/a-pretty-good-splash-screen-in-c

namespace SplashScreen
{
    public partial class SplashScreen : Form
    {
        //  Threading
        static public SplashScreen frmSplash = null;
        static private Thread oThread = null;

        //  Fade in and out
        public double dOpacityIncrement = .05;
        public double dOpacityDecrement = .1;
        private int iTimerInterval = 50;

        //  Status and progress bar
        private string sStatus;
        private double dCompletionFraction = 0;
        private Rectangle rProgress;

        //  Progress smoothing
        private double dLastCompletionFraction = 0.0;
        private double dPBIncrementPerTimerInterval = .0015;

        //  Self calibration support
        private bool bFirstLaunch = false;
        private DateTime dtStart;
        private bool bDTSet = false;
        private int iIndex = 1;
        private int iActualTicks = 0;
        private ArrayList alPreviousCompletionFraction;
        private ArrayList alActualTimes = new ArrayList();
        private const string REG_KEY_INITIALIZATION = "CalInitialization";
        private const string REGVALUE_PB_MILISECOND_INCREMENT = "CalIncrement";
        private const string REGVALUE_PB_PERCENTS = "CalPercents";

        //  Pausing support
        private static bool bPaused;
        private static DateTime dtPauseStarted = DateTime.MinValue;
        private static DateTime dtPauseStopped = DateTime.MinValue;

        //--------------------
        private Image imgBackGround = null;

        private Color cColor1 = Color.FromArgb(058, 096, 151);
        private Color cColor2 = Color.FromArgb(181, 237, 254);

        private bool bStarted = false;
        private bool bAllowClose = false;
        private bool bUserClosed = false;
        private bool bIntervalsStored = false;
        private int iMinDisplayTime = 0;

		//  Splash widgets
        private Label lblDismiss = null;
        private Label lblStatus = null;
        private Label lblRemaining = null;
        private Panel pnlStatus = null;
        private System.Windows.Forms.Timer oTimer = null;

        public SplashScreen()
        {
            InitializeComponent();

            this.Name = "SplashScreen";

            //Opacity = .0;
            FormBorderStyle = FormBorderStyle.None;
            StartPosition = FormStartPosition.CenterScreen;
            ShowInTaskbar = false;

            if (XmlAccess.GetString("Image") != "")
            {
                imgBackGround = Image.FromFile(XmlAccess.GetString("Image"));

                if (XmlAccess.GetString("TransparencyKey") != "")
                {
                    Opacity = 0;
                    dOpacityIncrement = XmlAccess.GetDouble("OpacityIncrement");
                    dOpacityDecrement = XmlAccess.GetDouble("OpacityDecrement");

                    Bitmap b = new Bitmap(imgBackGround);
                    b.MakeTransparent(XmlAccess.GetColor("TransparencyKey"));
                    BackColor = TransparencyKey = XmlAccess.GetColor("TransparencyKey");
                    BackgroundImage = b;
                }
                //  Disable fade-in fade-out effect when using images with alpha channel (transparent)
                else if (XmlAccess.GetInt("AlphaTransparency") == 1)
                {
                    Opacity = 1;
                    dOpacityIncrement = 1;
                    dOpacityDecrement = 1;
                }
                else
                {
                    Opacity = 0;
                    dOpacityIncrement = XmlAccess.GetDouble("OpacityIncrement");
                    dOpacityDecrement = XmlAccess.GetDouble("OpacityDecrement");
                }
                
                ClientSize = imgBackGround.Size;
            }
            else
            {
                ClientSize = XmlAccess.GetSize("SplashSize");
            }

            iTimerInterval = XmlAccess.GetInt("TimerInterval");
            iMinDisplayTime = XmlAccess.GetInt("MinDisplayTime");

            if (XmlAccess.GetInt("LabelShow") == 1)
            {
                lblStatus = new Label();
                lblStatus.Name = "lblStatus";
                lblStatus.Location = XmlAccess.GetPoint("LabelLocation");
                lblStatus.Size = XmlAccess.GetSize("LabelSize");
                lblStatus.Font = XmlAccess.GetFont("LabelFont");
                lblStatus.ForeColor = XmlAccess.GetColor("LabelFontColor");
                lblStatus.BackColor = XmlAccess.GetColor("LabelBackColor");
                this.Controls.Add(lblStatus);
            }

            if (XmlAccess.GetInt("RemainingShow") == 1)
            {
                lblRemaining = new Label();
                lblRemaining.Name = "lblRamaining";
                lblRemaining.Location = XmlAccess.GetPoint("RemainingLocation");
                lblRemaining.Size = XmlAccess.GetSize("RemainingSize");
                lblRemaining.Font = XmlAccess.GetFont("RemainingFont");
                lblRemaining.ForeColor = XmlAccess.GetColor("RemainingFontColor");
                lblRemaining.BackColor = XmlAccess.GetColor("RemainingBackColor");
                this.Controls.Add(lblRemaining);
            }

            if (XmlAccess.GetInt("ProgressShow") == 1)
            {
                pnlStatus = new Panel();
                pnlStatus.Name = "pnlStatus";
                pnlStatus.BackColor = XmlAccess.GetColor("ProgressBackColor");
                pnlStatus.Location = XmlAccess.GetPoint("ProgressLocation");
                pnlStatus.Size = XmlAccess.GetSize("ProgressSize");
                dPBIncrementPerTimerInterval = XmlAccess.GetDouble("ProgressIncrement");
                cColor1 = XmlAccess.GetColor("ProgressColor1");
                cColor2 = XmlAccess.GetColor("ProgressColor2");
                this.Controls.Add(pnlStatus);
            }

            if (XmlAccess.GetInt("DismissShow") == 1)
            {
                lblDismiss = new Label();
                lblDismiss.Name = "lblDismiss";
                lblDismiss.Cursor = Cursors.Hand;
                lblDismiss.Text = XmlAccess.GetString("DismissText");
                lblDismiss.Location = XmlAccess.GetPoint("DismissLocation");
                lblDismiss.Size = XmlAccess.GetSize("DismissSize");
                lblDismiss.Font = XmlAccess.GetFont("DismissFont");
                lblDismiss.ForeColor = XmlAccess.GetColor("DismissFontColor");
                lblDismiss.BackColor = XmlAccess.GetColor("DismissBackColor");
                lblDismiss.Click += new EventHandler(lblDismiss_Click);
                this.Controls.Add(lblDismiss);
            }

            oTimer = new System.Windows.Forms.Timer();
            oTimer.Tick += new EventHandler(ms_oTimer_Tick);
            oTimer.Interval = iTimerInterval;
            oTimer.Start();
        }

        #region "Static Methods"
        //  A static method to create the thread and launch the SplashScreen.
        static public void ShowSplashScreen(string ConfFile)
        {
            //  Make sure it's only launched once.
            if (frmSplash != null)
                return;

            XmlAccess.XmlFile = ConfFile;

            oThread = new Thread(new ThreadStart(SplashScreen.ShowForm));
            oThread.Name = "oThread";
            oThread.IsBackground = true;
            oThread.SetApartmentState(ApartmentState.STA);
            oThread.Start();

            while (frmSplash == null) ;
            while (frmSplash.bStarted == false) ;
            while (frmSplash.IsHandleCreated == false)
                System.Threading.Thread.Sleep(frmSplash.iTimerInterval);
        }

        //  A private entry point for the thread.
        static private void ShowForm()
        {
            frmSplash = new SplashScreen();
            frmSplash.Name = "frmSplash";
            Application.Run(frmSplash);
        }

        //  A static method to close the SplashScreen
        static public void CloseSplashScreen(IntPtr pHandle)
        {
            //  MethodInvoker SetForegroundWin = delegate { SetForegroundWindow(pHandle); };

            if (frmSplash != null && frmSplash.IsDisposed != true)
            {
                //if (frmSplash.InvokeRequired)
                //    frmSplash.Invoke(SetForegroundWin);
                //else
                //{
                //    if (pHandle != System.IntPtr.Zero)
                //        SetForegroundWin();
                //}

                frmSplash.bAllowClose = true;
                frmSplash.dOpacityIncrement = -frmSplash.dOpacityDecrement;
            }
            oThread = null;
            frmSplash = null;
        }

        //  A static method to set the status and update the reference.
        static public void SetStatus(string newStatus)
        {
            SetStatus(newStatus, true);
        }

        //  A static method to set the status and optionally update the reference.
        //  This is useful if you are in a section of code that has a variable
        //  set of status string updates.  In that case, don't set the reference.
        static public void SetStatus(string newStatus, bool setReference)
        {
            if (frmSplash == null)
                return;

            frmSplash.sStatus = newStatus;
            if (setReference)
                frmSplash.SetReferenceInternal();
        }

        //  Static method called from the initializing application to 
        //  give the splash screen reference points.  Not needed if
        //  you are using a lot of status strings.
        static public void SetReferencePoint()
        {
            if (frmSplash == null)
                return;
            frmSplash.SetReferenceInternal();
        }

        static public void Pause()
        {
            if (dtPauseStarted == DateTime.MinValue)
            {
                dtPauseStarted = DateTime.Now;
                bPaused = true;
            }
        }

        static public void Resume()
        {
            if (bPaused)
            {
                dtPauseStopped = DateTime.Now;
                bPaused = false;
            }
        }
        #endregion

        #region "Private Methods"
        //  Internal method for setting reference points.
        private void SetReferenceInternal()
        {
            if (bDTSet == false)
            {
                bDTSet = true;
                dtStart = DateTime.Now;
                ReadIncrements();
            }
            double dblMilliseconds = ElapsedMilliSeconds();
            alActualTimes.Add(dblMilliseconds);
            dLastCompletionFraction = dCompletionFraction;
            if (alPreviousCompletionFraction != null && iIndex < alPreviousCompletionFraction.Count)
                dCompletionFraction = (double)alPreviousCompletionFraction[iIndex++];
            else
                dCompletionFraction = (iIndex > 0) ? 1 : 0;
        }

        //  Utility function to return elapsed Milliseconds since the SplashScreen was launched.
        private double ElapsedMilliSeconds()
        {
            if (bPaused)
            {
                return (dtPauseStarted - dtStart).TotalMilliseconds;
            }
            else
            {
                if (dtPauseStarted == DateTime.MinValue)
                    return (DateTime.Now - dtStart).TotalMilliseconds;
                else
                    return (DateTime.Now - dtStart).TotalMilliseconds - (dtPauseStopped - dtPauseStarted).TotalMilliseconds;
            }
        }

        //  Function to read the checkpoint intervals from the previous invocation of the splashscreen from the registry.
        private void ReadIncrements()
        {
            double dblResult = XmlAccess.GetDouble(REGVALUE_PB_MILISECOND_INCREMENT);
            if (dblResult == 0) dblResult = .0015;
            dPBIncrementPerTimerInterval = dblResult;



            string sPBPreviousPctComplete = XmlAccess.GetString(REGVALUE_PB_PERCENTS);
            if (sPBPreviousPctComplete != "")
            {
                string[] aTimes = sPBPreviousPctComplete.Split(null);
                alPreviousCompletionFraction = new ArrayList();
                for (int i = 0; i < aTimes.Length; i++)
                {
                    double dblVal;
                    if (Double.TryParse(aTimes[i],
                                   System.Globalization.NumberStyles.Float,
                                   System.Globalization.NumberFormatInfo.InvariantInfo,
                                   out dblVal))
                        alPreviousCompletionFraction.Add(dblVal);
                    else
                        alPreviousCompletionFraction.Add(1.0);
                }
            }
            else
            {
                //  If this is the first launch, flag it so we don't try to show the scroll bar.
                bFirstLaunch = true;
            }
        }

        //  Method to store the intervals (in percent complete) from the current invocation of the splash screen to the registry.
        private void StoreIncrements()
        {
            if (bUserClosed != true && bIntervalsStored != true)
            {
                bIntervalsStored = true;

                string sPercent = "";
                double dblElapsedMS = ElapsedMilliSeconds();
                for (int i = 0; i < alActualTimes.Count; i++)
                    sPercent += ((double)alActualTimes[i] / dblElapsedMS).ToString(
                        "0.####", System.Globalization.NumberFormatInfo.InvariantInfo) + " ";

                XmlAccess.SetString(REGVALUE_PB_PERCENTS, sPercent);

                dPBIncrementPerTimerInterval = 1.0 / (double)iActualTicks;
                XmlAccess.SetDouble(REGVALUE_PB_MILISECOND_INCREMENT, dPBIncrementPerTimerInterval);
            }
        }
        #endregion

        #region "Event Handlers"
        //  Tick Event handler for the Timer control.  
        //  Handle fade in and fade out.
        //  Also handle the smoothed progress bar.
        void ms_oTimer_Tick(object sender, EventArgs e)
        {
            //  If we are currently paused, take no action
            if (bPaused && !bUserClosed)
                return;

            if (lblStatus != null)
                lblStatus.Text = sStatus;

            if (dOpacityIncrement > 0)
            {
                if (bDTSet)
                    iActualTicks++;
                if (this.Opacity < 1)
                    this.Opacity += dOpacityIncrement;
            }
            else
            {
                StoreIncrements();
                if ((DateTime.Now - dtStart).TotalMilliseconds > iMinDisplayTime)
                {
                    if (this.Opacity > 0)
                        this.Opacity += dOpacityIncrement;
                    else
                    {
                        oTimer.Stop();
                        
                        this.Close();
                    }
                }
            }

            if (pnlStatus != null)
            {
                if (bFirstLaunch == false && dLastCompletionFraction < dCompletionFraction)
                {
                    pnlStatus.Visible = true;
                    dLastCompletionFraction += dPBIncrementPerTimerInterval;
                    int width = (int)Math.Floor(pnlStatus.ClientRectangle.Width * dLastCompletionFraction);
                    int height = pnlStatus.ClientRectangle.Height;
                    int x = pnlStatus.ClientRectangle.X;
                    int y = pnlStatus.ClientRectangle.Y;
                    if (width > 0 && height > 0)
                    {
                        rProgress = new Rectangle(x, y, width, height);
                        if (!IsDisposed)
                        {
                            Graphics g = pnlStatus.CreateGraphics();
                            LinearGradientBrush brBackground = new LinearGradientBrush(rProgress, cColor1, cColor2, LinearGradientMode.Horizontal);
                            g.FillRectangle(brBackground, rProgress);
                            g.Dispose();
                            brBackground.Dispose();
                        }


                        if (lblRemaining != null)
                        {
                            int iSecondsLeft = 1 + (int)(iTimerInterval * ((1.0 - dLastCompletionFraction) / dPBIncrementPerTimerInterval)) / 1000;
                            if (iSecondsLeft == 1)
                                lblRemaining.Text = string.Format("1 second remaining");
                            else
                                lblRemaining.Text = string.Format("{0} seconds remaining", iSecondsLeft);
                        }
                    }
                }
                else
                {
                    pnlStatus.Visible = false;
                }
            }
        }

        //  Close the form if label dismiss clicked.
        void lblDismiss_Click(object sender, EventArgs e)
        {
            bUserClosed = true;
            CloseSplashScreen(System.IntPtr.Zero);
        }
        #endregion


        protected override void OnPaint(PaintEventArgs e)
        {
            if (frmSplash != null)
                frmSplash.bStarted = true;
        }

        protected override void OnPaintBackground(PaintEventArgs e)
        {
            if (XmlAccess.GetInt("AlphaTransparency") == 0)
                base.OnPaintBackground(e);
            else
                e.Graphics.DrawImage(imgBackGround, new Rectangle(0, 0, imgBackGround.Width, imgBackGround.Height));
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            base.OnClosing(e);
            e.Cancel = !bAllowClose;
        }
    }
}
